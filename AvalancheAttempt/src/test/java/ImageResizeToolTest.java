import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;


public class ImageResizeToolTest {
    private String actual;
    private String expected;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void getResizedEncodedImage() throws Exception {//Test for different unexpectable situations
        thrown.expect(wrongFilePathExceptions.class);


        actual = ImageResizeTool.getResizedEncodedImage("","directory",200);
        actual = ImageResizeTool.getResizedEncodedImage("https://www.elnet.ee/pildinaited/cover_images/789949526765_orig.jpg",
                "url",200);
        actual = ImageResizeTool.getResizedEncodedImage("www.elnet.ee",
                "url",200);
        actual = ImageResizeTool.getResizedEncodedImage("https://www.elnet.ee/pilg",
                "url",200);
        actual = ImageResizeTool.getResizedEncodedImage("https://wwweee",
                "url",200);
        actual = ImageResizeTool.getResizedEncodedImage("www.elnet.ee/pildinaited/cover_images/bibimage_test/isbn_9789949526765_orig.jpg",
                "directory",200);
        actual = ImageResizeTool.getResizedEncodedImage("www.elnet.ee/pildinaited/cover_images/bibimage_test/isbn_9789949526765_orig.jpg",
                "base64",200);
        actual = ImageResizeTool.getResizedEncodedImage("www.elnete/pildinaited/cover_images/bibimage_test/isbn_9789949526765_orig.jpg",
                "bas",200);
        thrown.expect(wrongInputExceptions.class);
        actual = ImageResizeTool.getResizedEncodedImage("www.elnet.ee/pildinaited/cover_images/bibimage_test/isbn_9789949526765_orig.jpg",
                "base64",0);
        actual = ImageResizeTool.getResizedEncodedImage("www.elnet.ee/pildinaited/cover_images/bibimage_test/isbn_9789949526765_orig.jpg",
                "bas",200);
        actual = ImageResizeTool.getResizedEncodedImage("",
                "base64",0);
        actual = ImageResizeTool.getResizedEncodedImage("",
                "",-1);



    }



    @Test
    public void realExamplesWorksTest() throws Exception {
        try {

            expected = new String(Files.readAllBytes(Paths.get("src/test/resources/encodedResizedExample.txt")));


        } catch (IOException e) {
            e.printStackTrace();
        }
        actual = ImageResizeTool.getResizedEncodedImage(
                "https://www.elnet.ee/pildinaited/cover_images/bibimage_test/isbn_9789949526765_orig.jpg",
                "URL",500);
        assertEquals(expected, actual);//test for URL

        actual = ImageResizeTool.getResizedEncodedImage(
                new String(Files.readAllBytes(Paths.get("src/test/resources/encodedExample.txt"))),
                "base64",500);
        assertEquals(expected, actual);// test for base64
        actual = ImageResizeTool.getResizedEncodedImage(
                "src/test/resources/example.jpg",
                "directory",500);
        assertEquals(expected, actual);//test for directory
    }






}
