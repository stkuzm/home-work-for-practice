import sun.awt.image.ToolkitImage;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;

class ImageResizeTool {


    private static ImageIcon imageDecoder(String base64Image) throws IOException {// Image String to Image Decoder
        byte[] imageByteArray = Base64.getDecoder().decode(base64Image); //get decoded message to byte array
        return new ImageIcon(ImageIO.read(new ByteArrayInputStream(imageByteArray)));
    }

    private static String ImageEncoder(ImageIcon Icon) throws IOException{
        Image image = Icon.getImage(); // Image to buffered image
        BufferedImage bufferedImage = ((ToolkitImage) image).getBufferedImage(); // Image to buffered image
        ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "jpg", byteArrayOutputStream );
        byte [] data = byteArrayOutputStream.toByteArray(); //encoding to byte array
        return Base64.getEncoder().encodeToString(data); // returning byte array in string
    }

    private static ImageIcon imageResize(String filePath, String pathType,int neededHeight) throws IOException,wrongFilePathExceptions{ //get resized Image from original
        ImageIcon originalIcon = imageDeliveryController(filePath, pathType);
        float neededWidth = (float)originalIcon.getIconWidth() / (float)originalIcon.getIconHeight() * neededHeight;//new width creating
        Image originalImage = originalIcon.getImage();

        if((int)neededWidth == 0)//avoidance of the IllegalArgumentException
            neededWidth = 1;

        Image resizedImage = originalImage.getScaledInstance((int)neededWidth,neededHeight, Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

    private static ImageIcon imageDeliveryController(String filePath, String pathType) throws IOException, wrongFilePathExceptions {

        ImageIcon originalIcon = null;
        switch (pathType.toLowerCase()) {
            case "url": {
                try {
                    originalIcon = new ImageIcon(ImageIO.read(new URL(filePath)));
                    break;
                } catch (IIOException e){
                    throw new wrongFilePathExceptions("URL is wrong or image does not exist!");
                }

            }
            case "directory": {
                try {
                    originalIcon = new ImageIcon(ImageIO.read(new File(filePath)));
                    break;
                } catch (IIOException e){
                    throw new wrongFilePathExceptions("Directory or file does not exist!");
                }
            }
            case "base64": {
                try {
                    originalIcon = imageDecoder(filePath);
                    break;
                } catch(NullPointerException npe){
                    throw new wrongFilePathExceptions("Base64 string is wrong!");

                }
            }
        }
        if (originalIcon == null){
            System.exit(1);
        }
        return originalIcon;
    }

    static String getResizedEncodedImage(String filePath, String pathType,int neededHeight) throws IOException,wrongFilePathExceptions{//main void

         try {
             if(neededHeight < 1) {
                 throw new wrongInputExceptions("NeededHeight param must be bigger than 0");
             }
             else if(!pathType.toLowerCase().equals("url") && !pathType.toLowerCase().equals("directory")
                     && !pathType.toLowerCase().equals("base64")) {
                 throw new wrongInputExceptions("pathType param must be 'URL', 'Directory' or 'Base64'!");
             }
             else {
                 return ImageEncoder(imageResize(filePath, pathType, neededHeight));
             }
         } catch (wrongInputExceptions wrongInputExceptions) {
             wrongInputExceptions.printStackTrace();
             return "";
         }


     }

}
